\documentclass[final]{nddiss2e}
                     % One of the options draft, review, final must be chosen.
                     % One of the options textrefs or numrefs should be chosen
                     % to specify if you want numerical or ``author-date''
                     % style citations.
                     % Other available options are:
                     % 10pt/11pt/12pt (available with draft only)
                     % twoadvisors
                     % noinfo (should be used when you compile the final time
                     %         for formal submission)
                     % sort (sorts multiple citations in the order that they're
                     %       listed in the bibliography)
                     % compress (compresses numerical citations, e.g. [1,2,3]
                     %           becomes [1-3]; has no effect when used with
                     %           the textrefs option)
                     % sort&compress (sorts and compresses numerical citations;
                     %           is identical to sort when used with textrefs)

\usepackage{makeidx}
\makeindex

\usepackage{pdflscape}

\usepackage{tikz}
\hypersetup{hidelinks} % Get rid of boxes around links

\setlength{\textfloatsep}{36pt} % Triple-space between table and text
\newcommand{\generalnote}[1]{\vspace*{\abovefigureskip}#1}

% nddiss doesn't make figure* and table* conform to the ND guidelines,
% so redefine them to be the same as figure and table
\renewenvironment{figure*}{\begin{figure}}{\end{figure}}
\renewenvironment{table*}{\begin{table}}{\end{table}}

\begin{document}

\frontmatter             % All the items before Chapter 1 go in ``frontmatter''

\title{ A Generic NLP Thesis }  % Title

\author{ Jane Doe }      % Author's name
\work{Dissertation}    % ``Dissertation'' or ``Thesis''. Don't surround with spaces.
\degaward{ Doctor of Philosophy }  % Degree you're aiming for.
                                   % Should be one of the following options:
                                   % ``Doctor of Philosophy'' (do NOT include ``in Subject'')
                                   % ``Master of Science \\ in \\ Subject''
\advisor{David Chiang}  % Advisor's name. Don't surround with whitespace
 % \secondadvisor{ }     % Second advisor, if used option ``twoadvisors''
\department{Computer Science and Engineering}           % Name of the department. Don't surround with spaces.

\maketitle               % The title page is created now

 % You must use either the \makecopyright option or the \makepublicdomain option.
 % \copyrightholder{ }   % If you're not the copyright holder
 % \copyrightyear{ }     % If the copyright is not for the current year
 % \makecopyright        % If not making your work public domain
                         % uncomment out \makecopyright
 % \makepublicdomain     % Uncomment this to make your work public domain

 % Including an abstract is optional for a master's thesis, and required for a
 % doctoral dissertation.
\begin{abstract}
Translation is hard. But we built a system that is so much better than everyone else's.
\end{abstract}

 % Including a dedication is optional.
 % \renewcommand{\dedicationname}{\mbox{}} % Replace \mbox{} if you want
                                           % something else.
 % \begin{dedication}
 % \end{dedication}

\tableofcontents
\listoffigures
\listoftables

 % Including a list of symbols is optional.
 %% \renewcommand{\symbolsname}{newsymname} % Replace ``newsymname'' with
                                            % the name you want, and uncomment
 % \begin{symbols}
 % \end{symbols}

 % Including a preface is optional.
 %% \renewcommand{\prefacename}{ } % If you want another Preface name, add
                                   % something else, and uncomment.
 % \begin{preface}
 % \end{preface}

 % Including an acknowledgements section may or may not be optional. It's hard to
 % tell from the information available in Spring 2013.
 %% \renewcommand{\acknowledgename}{ } % If you want another Acknowledgement name
                                       % add something else, and uncomment
\begin{acknowledge}
  You can write anything you want in the acknowledgements section. But it should also include a statement of funding: probably a union of the acknowledgements sections of all the papers you've written.
  
  The research described in this dissertation was supported in part by US Government Grant 123456.
\end{acknowledge}

\mainmatter

\chapter{Introduction}

The introduction is the most important chapter of the thesis. Its purpose is to state the contributions of the thesis and their significance as clearly as possible, while introducing as little technical detail as possible.

Start with a wide-angle view (e.g., what is machine translation\index{machine translation} and what is it used for?) and take your time zooming in to the particular problem you work on. Then give a high-level overview of the contributions you have made to this problem.

I am not sure where the custom arose of writing an explicit Thesis Statement, but I'm personally not a fan of it. You should be able to make it clear what the ``thesis of your thesis'' is without putting a label on it.

\chapter{A Meta Chapter}

There should be some text between the chapter heading and the first section heading. It should be short and it should introduce the chapter. Some people like to put an epigraph (a quotation somehow related to the chapter) here.

\section{A Section}

There should be some between a section heading and the first subsection heading. But it should be short; substantial content should be made into its own subsection.

\subsection{A subsection}

Use consistent capitalization in chapter/section/subsection titles: either title or sentence capitalization, but not a mix. I read somewhere that sections should use title capitalization and subsections and below should use sentence capitalization, as shown here.

Itemized and enumerated lists are single-spaced:
\begin{itemize}
\item This is a very very very very very very very very very very very very very very long item.
\item Second item.
\end{itemize}

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur \citep{brown-etal-1993-mathematics}. 

\begin{table}[t] % Table should be at the top of page
% The table caption goes above the table. Because it appears in all caps, keep it short. It shouldn't have final punctuation.
\caption{Example table}
\label{tab:results}
\begin{center}
\begin{tabular}{lc}
\toprule\midrule % Two lines at the top of the table
System & BLEU (\%) \\
\midrule
Theirs & 12.3 \\
Ours & 34.5 \\
\bottomrule
\end{tabular}
\end{center}

% You can put a longer explanation as a "general note" below the table. We define the macro \generalnote for this.
\generalnote{See the source code for more information about tables. There are a lot of requirements for tables that are different from usual conference papers.}
\end{table}

\begin{figure}[t] % Figure should be at the top of page
\begin{center}
\begin{tikzpicture}
\draw (0,0) circle (1);
\end{tikzpicture}
\end{center}
% Use the optional argument to provide a short caption that will appear in the list of figures.
% Captions go below the figure (unlike table captions)
% Caption alignment should be consistent for all figures in all chapters (either left aligned or centered)
\caption[Our system]{Our system is so simple, it's like this empty circle. Again, please see the source code for more information about figures.}
\label{fig:system}
\end{figure}
% If using multiple figures, they should be placed vertically, do not place them side-by-side

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

\begin{landscape}
% For especially large tables, you can use pdflscape to rotate them and give them their own page
\begin{table}
\begin{threeparttable}[b]
\caption{Machine translation experiments with various position encodings}
\label{tab:position}
\newcommand{\sig}{\rlap{$^\dag$}}
\begin{center}
\begin{tabularx}{1.25\textwidth}{@{}lllllllll|l@{}} % When pdflscape rotates, it messes up \textwidth vs \textheight
\toprule
& & \multicolumn{8}{c}{case-insensitive BLEU} \\
Model & Training & En-Vi$^\ast$ & Uz-En & Ha-En & Hu-En & Ur-En & Ta-En & Tu-En & combined \\
\midrule
diagonal polar & fixed & 32.6 & 25.7 & 24.4 & \textbf{34.2} & 11.5 & 13.4 & 25.7 & 26.4 \\
& learned angles & \textbf{32.7} & 25.8 & 25.4\sig & 34.0 & 11.1\sig & 14.1\sig & 25.7 & \textbf{26.6} \\
\midrule
full matrix & random & 32.6 & \textbf{25.9} & \textbf{25.6}\sig & 34.1 & 11.1\sig & 12.6\sig & \textbf{26.1} & 26.5 \\
& learned & 32.5 & 24.5\sig & 23.6 & 33.5 & 11.4 & \textbf{14.5}\sig & 23.8\sig & 26.5 \\
\midrule
per position & random & 32.6 & 24.3\sig & 24.6 & 33.6\sig & 11.1 & 14.0\sig & 25.7 & 26.3 \\
& learned & 32.0\sig & 22.6\sig & 21.2\sig & 33.0\sig & \textbf{11.7} & 14.4\sig & 21.1\sig & 25.0\sig \\
\bottomrule
\end{tabularx}

\begin{tablenotes}
\item This table is too large to fit properly within the text.  You can use landscape mode from the pdflscape package to rotate the table.
\end{tablenotes}
\end{center}
\end{threeparttable}\\
$^\ast$tokenized references \\
$^\dag$significantly different from first line ($p<0.05$, bootstrap resampling)
\end{table}
\end{landscape}

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

\chapter{Background}

It's pretty typical for the second chapter to collect together background material that is shared by many chapters. (If some background is needed by a single chapter, maybe it's better to put it in that chapter.) For example, this is where you'd describe baseline machine translation\index{machine translation} systems you compare against, or basic definitions that you use throughout.

\chapter{A Content Chapter}

It's true that there is usually a one-to-one mapping between conference papers and content chapters. But, it doesn't have to be that way. You should feel free to split, merge, and reorder your previous papers in any way to produce the most coherent presentation possible (not: the shortest writing time possible).

In any case, a content chapter should not have the structure of a conference paper. It should not have an abstract. It should never say ``in this paper'' anywhere or refer to other chapters as ``in previous work.''

On the other hand, I think it would be appropriate (maybe in a footnote, or maybe here in the text above the first section), to note which conference paper the chapter draws from, and if the paper was co-authored with anyone else, to say anything that needs to be said about who did what.

\chapter{Another Content Chapter}

That's right.

\chapter{Yet Another Content Chapter}

There's usually at least three, but there can be as many as you want.

\chapter{Conclusion}

The conclusion is the second most important chapter of the thesis. It should be more than a page long. Maybe five pages is good, though in other disciplines they can be far longer. It should recapitulate the contributions of the thesis, and it can do so in a way that leans on technical details more heavily than the introduction can. It should synthesize the individual contributions into larger findings. It should discuss larger implications of the work, like how it should affect future research in its area. 

\appendix

 % If you have appendices, add them here.
 % Begin each one with \chapter{TITLE} as before. The \appendix command takes
 % care of renaming chapter headings and creates a new page in the Table of
 % Contents for them.

\backmatter              % Place for bibliography and index

\bibliographystyle{nddiss2e}
\bibliography{diss}           % input the bib-database file name

\printindex

\end{document}
